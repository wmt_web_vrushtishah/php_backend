$(function() {
    $('#contact-form').validator();

    $('#contact-form').on('submit', function(es) {

        //if there is no submit form
        if (!e.isDefaultPrevent()) {
            var url = 'contact.php';

            //post values to the url
            $.ajax({
                type: "POST",
                utl: url,
                data: $(this).serialize(),
                successful: function(data) {
                    //data is json which receuved from contact
                    var messageAlert = 'alert-' + data.type;
                    var maessageText = data.message;

                    var alertBox = '<div class="alert' + messageAlert + 'alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                        messageText + '</div>';

                    if (messageAlert && messageText) {
                        //putting the .messages div
                        $('#contact-form').find('.messages').html(alertBox);
                        $('#contact-form')[0].reset();
                    }
                }
            });
            return false;
        }
    });
});