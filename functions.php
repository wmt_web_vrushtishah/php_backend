<?php

function hello_world(){
    echo "Hello Welcome!!!! <br>";
    echo "Nice to meet you <br>";
}
hello_world();

//function with arguments
function add($num1,$num2){
    echo 'Addition is: ';
    echo $num1 + $num2 . '<br>';
}

add(45,7);


//function with returning the value
function add1($num1,$num2){
    $result= $num1 + $num2 . '<br>';
    return $result;
}

$num3=add1(90,7);
echo add1(90,7);
$num4=add1(40,47);
echo add1(40,47);
echo $ans=($num3 * $num4).'<br>';

//date and time function
$date =date('d/m/Y');
echo $date.'<br>';

$time=date('H-i-s');
echo $time;
?>