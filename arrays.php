<?php

$names=array('Harry'=>24,'Potter'=>67,'Tom'=>45,'Jerry'=>9 );

$names[4]='Mickey';
$names[5]='Mini';
// echo $names[4];
// echo 'Age is: '.$names['Harry'].'<br>'; //associatve values
// print_r($names);//would give all the values of the array


//Multidimensional Array
/* students
name    age    weight
Harry   24     78
Potter  67     90
Tom     45     60
Jerry    9     89 */

$students=array(

 "student1"=>array(
                0=>' Harry ',
                1=>24,
                2=>78),

    "student2"=>array(
                0=>' Potter ',
                1=>67,
                2=>90),

    "student3"=>array(
                0=>' Tom ',
                1=>45,
                2=>60),

    "student4"=>array(
                0=>' Jerry ',
                1=>9,
                2=>89)
            );

foreach($students as $innerArray)
{
    //check type
    if(is_array($innerArray)){
        //scan the inner loop
        foreach($innerArray as $value){
            echo '<pre>';
            print_r ($value);
        }
    }else{
        echo '<pre>';
        print_r ($innerArray);
    }
}

//echo $students["student4"][0];
?>