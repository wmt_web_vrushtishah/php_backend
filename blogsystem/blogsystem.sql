-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 19, 2021 at 02:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Food'),
(2, 'It'),
(3, 'Music'),
(4, 'Travel');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(3, 'homonyms/others', 'Welcome and learn about the homonyms which would be very useful to veryone who are new learners', '2021-01-18 06:13:14', '2021-01-18 06:13:14'),
(4, 'homonyms', 'dyohjdpks[pkdhihoj[ldhgokvcxtyggihokjokjohifggghqerttuyoiipkl,m,ln,nvnvxzxfx', '2021-01-18 07:09:44', '2021-01-18 07:09:44'),
(7, 'Testing.....', 'hello welcome to testing part as its in trending now a days..........', '2021-01-18 04:58:16', '2021-01-18 04:58:16'),
(9, 'Movies', 'you should watch the movie its amazing.....', '2021-01-18 04:58:16', '2021-01-18 04:58:16'),
(10, '100 top dishes', 'who love to try different types of dishes this blog will help them ....', '2021-01-18 04:58:16', '2021-01-18 04:58:16'),
(11, 'testing useful languages', 'pyhton database', '2021-01-18 04:58:16', '2021-01-18 04:58:16'),
(12, 'on going current situation in IT companies', 'vgihhjhkojlk;gfyty576787980fbbkjk];l[;456856222222222554200561727vashklsjiwbnb---------', '2021-01-18 05:22:48', '2021-01-18 05:19:46'),
(13, 'hdkllngff', 'fytfuhjkjnkbkajfhojofk', '2021-01-18 06:03:15', '2021-01-18 06:03:15'),
(15, 'defghjk', 'vcksjnklmlmnajbvhvsugvgvud', '2021-01-18 07:13:20', '2021-01-18 07:13:20'),
(16, 'homonyms', 'aaaa bbbbbb cccccc ddddd', '2021-01-18 07:12:21', '2021-01-18 07:12:21'),
(18, 'my self', 'vrushti shah', '2021-01-18 09:45:45', '2021-01-18 09:45:45'),
(19, '100 top music', 'welcome', '2021-01-18 10:27:40', '2021-01-18 10:27:40'),
(20, 'aaa bbbbb ', '1234567890123456789', '2021-01-18 10:25:03', '2021-01-18 10:25:03'),
(21, '100 top dishes', 'asdfghjklpoiuytrewqzxcvbnm', '2021-01-18 10:26:06', '2021-01-18 10:26:06'),
(22, 'multiple selection', 'is it done??', '2021-01-18 10:51:12', '2021-01-18 10:51:12'),
(23, 'testing useful languages', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2021-01-18 12:24:37', '2021-01-18 12:24:37'),
(24, 'homonyms/others', 'wqreytuhjkl,nmgvnkcjxdgchkb', '2021-01-18 12:30:26', '2021-01-18 12:30:26'),
(25, 'acfagg', 'dfdghh', '2021-01-18 12:51:11', '2021-01-18 12:51:11'),
(28, 'hgicfhko', 'afb;j;fko', '2021-01-18 12:54:16', '2021-01-18 12:54:16'),
(29, 'asfadg', 'htuyijyi', '2021-01-18 12:59:38', '2021-01-18 12:59:38'),
(31, 'testing useful languages', 'qwertyuiopasdfhgjklzxcvbnm', '2021-01-19 05:13:58', '2021-01-19 05:13:58'),
(32, 'hdkllngff', 'dagsyutdui', '2021-01-19 05:34:44', '2021-01-19 05:34:44'),
(33, 'testing useful languages', 'fgwuiehfokdpl', '2021-01-19 05:49:47', '2021-01-19 05:49:47'),
(34, 'Movies', 'SFeagsrthtjfyik', '2021-01-19 05:51:39', '2021-01-19 05:51:39'),
(35, 'all selected', 'plz do it', '2021-01-19 06:05:56', '2021-01-19 06:05:56'),
(36, '100 top dishes', 'shdijfytioygoluolu', '2021-01-19 06:07:14', '2021-01-19 06:07:14'),
(38, 'htuduj', 'erswe', '2021-01-19 07:51:57', '2021-01-19 07:51:57'),
(40, 'htuduj', 'erswe', '2021-01-19 07:52:46', '2021-01-19 07:52:46'),
(42, 'htuduj', 'erswe', '2021-01-19 07:53:55', '2021-01-19 07:53:55'),
(44, 'htuduj', 'erswe', '2021-01-19 08:03:38', '2021-01-19 08:03:38'),
(46, 'htuduj', 'erswe', '2021-01-19 08:06:43', '2021-01-19 08:06:43'),
(47, 'sdygsry', 'agyijyo', '2021-01-19 08:09:01', '2021-01-19 08:09:01'),
(48, 'sdygsry', 'agyijyo', '2021-01-19 08:20:28', '2021-01-19 08:20:28'),
(49, 'sdygsry', 'agyijyo', '2021-01-19 08:20:45', '2021-01-19 08:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `postcategories`
--

CREATE TABLE `postcategories` (
  `id` int(11) NOT NULL,
  `catname` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `postcategories`
--

INSERT INTO `postcategories` (`id`, `catname`) VALUES
(4, 'IT'),
(3, 'Music'),
(1, 'Travel'),
(2, 'Travel');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('Author','Admin') NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `role`, `password`, `created_at`, `updated_at`) VALUES
(1, 'rishikashah', 'rishika@yahoo.com', 'Author', 'Shah1234', '2021-01-14 12:07:49', '2021-01-14 12:07:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postcategories`
--
ALTER TABLE `postcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_foreign_key_name` (`catname`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `postcategories`
--
ALTER TABLE `postcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `postcategories`
--
ALTER TABLE `postcategories`
  ADD CONSTRAINT `fk_foreign_key_name` FOREIGN KEY (`catname`) REFERENCES `categories` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
