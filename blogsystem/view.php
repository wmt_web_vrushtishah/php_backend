<?php
include "logic.php";
// echo '<pre>';
// print_r($row);
// exit;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<title></title>
</head>
<body>
	<div class="container mt-5">
			<div>
				<div class="bg-dark p-5 rounded-lg text-white text-center">
				<h1><?php echo $row['title'];?></h1>
			</div>
			<p class="mt-5 border-left border-dark pl-3"><?php echo $row['content'];?></p>
			<div class="d-flex mt-2 justify-content-center align-items-center ">
				<a href="edit.php?id=<?php echo $row['id'];?>" class="btn btn-primary btn-sm">Edit</a>
				<form method="POST">
					<input type="hidden" class="delete_id_value" value="<?php echo $row["id"];?>">
					<button class="deletebtn btn-sm ml-2 btn btn-danger" name="delete">Delete</button>
				</form>
			</div>
	</div>

	<!-- Bootstrap js -->
	<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pooper.js@1.16.1/dist/umd/pooper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script>
		$(document).ready(function(){
			$('deletebtn').click(function(e){
				e.preventDefault();
				var deleteid = $(this).closest("form").find('.delete_id_value').val();
				//console.log(deleteid);
				swal({
 					 title: "Are you sure?",
  					text: "Once deleted, you will not be able to recover this data!",
  					icon: "warning",
  					buttons: true,
  					dangerMode: true,
				})
				.then((willDelete) => {
  					if (willDelete) {
						$.ajax({
							type:"POST",
							url:"blog.php",
							data:{
								"delete_btn_set":1,
								"delete_id":deleteid,
							},
							success:function(response){
								swal("Data deleted successfully!!",{
									icon:"success",
								}).then((result)=>{
									location.reload();
								});
							}
						});
					}
				});
			});
		});
	</script>
</body>
</html>