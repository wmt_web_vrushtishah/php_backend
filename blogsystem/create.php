<?php
include "logic.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<title></title>
</head>
<body>
	<div class="container mt-5">
		<form action="" method="POST">
			<h3 class='container bg-dark text-ceter p-3 text-warning rounded-lg mt-5'>Welcome To Add New Post</h3>
			<h2>Select Categories</h2>
				<input type="checkbox" id="checkItem" name="category" value="Music"><label>Music</label><br>
				<input type="checkbox" id="checkItem" name="category" value="Food"><label>Food</label><br> 
				<input type="checkbox" id="checkItem" name="category" value="Travel"><label>Travel</label><br>
			    <input type="checkbox" id="checkItem" name="category" value="IT"><label>IT</label><br>
			<input type="text" name="title" placeholder="Blog Title" class="form-control bg-dark text-white my-3 text-center" required="">
			<textarea name="content" class="form-control bg-dark text-white my-3 text-center" required=""></textarea>
			<button name="new_post" type="submit" class="btn btn-dark">Add Post</button>
		</form>
	</div>

	<!-- <!-- Bootstrap js -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pooper.js@1.16.1/dist/umd/pooper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>

